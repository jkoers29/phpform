  
	        <h2>Thank you for submitting <em><?php echo htmlentities($_POST['title']);?></em></h2>	        
	        
	        <h3>Ingredients</h3>
	        <ul>
			<?php foreach($_POST['ingredients'] as $row){?>
		
	        	<li><?php echo htmlentities($row);?></li>
			<?php } ?>
	        </ul>
	        
	        <h3>Instructions</h3>
	        <p><?php echo htmlentities($_POST['instructions']); ?></p>	
		
<a href="index.php">Return to recipe list</a>		
        
        