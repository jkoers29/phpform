   <a href="index.php?action=insert">Insert a recipe</a>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>                  
                <th></th>
                <th>Ingredients</th>
                <th></th>
                <th>Instructions</th>
            </tr>
        </thead>
        <tbody>
		
        <?php
			if(empty($this->data))
			{echo"<p>There's nothing in the array.....</p>";}
			else{
			foreach ($this->data as $recipe) { ?>     
            <tr>
                <td><?php echo htmlentities($recipe->id); ?></td>
                <td><?php echo htmlentities($recipe->title); ?></td>
					<?php foreach($recipe->ingredients as $ing){ ?>
						<td><?php echo htmlentities($ing->name); ?></td>
					<?php } ?>
                <td><?php echo htmlentities($recipe->instructions); ?></td>
            </tr>                                
        <?php }} ?> 
        </tbody>                
    </table> 
<form id="myForm" action="index.php" method="delete">
	<input class="input-large" type="text" placeholder="Enter index of recipe to" name="id">
	<input type="submit" value="Delete" onClick="delete">	
	</form>
	
		        	