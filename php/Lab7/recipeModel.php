<?php
include_once "model.php";
include_once "connection.php";
include_once "ingredients.php";

class Recipe
{
	function __construct($id, $title, $ingredients, $instructions)
	{
		$this->id = $id;
		$this->title = $title;
		$this->ingredients = $ingredients;
		$this->instructions = $instructions;
	}
}

class RecipeModel extends Model
{
	
	function findAll()
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		$query = "Select * from recipes;";
		if($res = $conn->query($query))
		{
			$result = array();
			while($row = $res->fetch_assoc())
			{
				$ingredient = new IngredientModel();
				$ingredients = $ingredient->find($row['id']);
				$result[] = new Recipe($row['id'], $row['title'], $ingredients, $row['instructions']);
			}
			
			return $result;
		}
	}

	function insert($title, $inst)
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		$query = $conn->prepare("Insert Into recipes (title, instructions)  Values (?, ?)");
		$query->bind_param('ss', $title, $inst);
		$query->execute();
		return $conn->insert_id;
	}
	function delete($id)
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		$query = "SELECT ing_id FROM rec_ing WHERE rec_id = $id;";
		if($res = $conn->query($query))
		{
			$result = array();
			while($row = $res->fetch_assoc())
			{
				$result[] = ($row['ing_id']);
			}
		}
		foreach($result as $row)
		{
			$answer = mysqli_query("DELETE from ingredients WHERE id = $row;");
			
		}
		$query2 = "DELETE from recipes WHERE id = $id;";
		if($res = $conn->query($query2)) {}
	}
}