<?php 

class Ingredients
{
	function __construct($name)
	{
		$this->name = $name;
	}
}

class IngredientModel extends Model
{
	function find($id)
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		$query = "SELECT ingredients.name from ingredients, rec_ing where ingredients.id = rec_ing.ing_id and rec_id = $id;";
		if($res = $conn->query($query))
		{
			$result = array();
			while($row = $res->fetch_assoc())
			{
				$result[] = new Ingredients($row['name']);			
			}
			return $result;
		}
	}
	function insert($ingredients)
	{	
		$arr = array();
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		foreach($ingredients as $row)
		{
			$query = $conn->prepare("Insert Into ingredients (name)  Values (?)");
			$query->bind_param('s', $row);
			$query->execute();
			$arr[] = $conn->insert_id;
		}
		
		return $arr;
	}
	function insertRecipeIngredient($recId, $ingId)
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		foreach($ingId as $row)
		{
			$query = $conn->prepare("Insert Into rec_ing (rec_id, ing_id)  Values (?, ?)");
			$query->bind_param('ii', $recId, $row);
			$query->execute();
		}
	}
	function deleteIngredient($id)
	{
		
	}
}