<?php
include_once "recipeModel.php";
include_once "view.php";
include_once "ingredients.php";

$recipeList = new RecipeModel();
$ingredientList = new IngredientModel();
$ingredientId = array();

if(isset($_GET['action']) && $_GET['action'] == "insert")
{
	$page = 'form';
	$data = null;
}
elseif($_SERVER['REQUEST_METHOD'] == "POST")
{
	$page = 'thanks';
	$data = null;
	$recipeId = $recipeList->insert($_POST['title'], $_POST['instructions']);
	$ingredientId = $ingredientList->insert($_POST['ingredients']);
	$ingredientList->insertRecipeIngredient($recipeId, $ingredientId);
}
elseif($_SERVER['REQUEST_METHOD'] == "DELETE")
{
	$recipeList->delete($_POST['id']);
}
else
{
	$page = 'list';
	$data = $recipeList->findAll();
}
$view = new View($page, $data);
$view->render($view);
?>