<form id="myForm" action="index.php" method="post">
<fieldset>
<legend>Insert a recipe</legend>
						
<label>Title</label>
<input class="input-xxlarge" type="text" placeholder="Spaghetti" name="title">
						
<label>Ingredients</label>
<div id ="ingredients"><input class="input-xlarge" type="text" placeholder="Ingredient" name="ingredients[]"></div>
<input type="button" value="Add another recipe" onClick="addInput('ingredients');">
<script>
	var counter = 1;
	var limit = 10;
	function addInput(divName)
	{
		if(counter == limit)
		{
			alert("You have reached the limit.");
		}
		else
		{
			var newdiv = document.createElement('ingredients');
			newdiv.innerHTML = "<br>Entry " + (counter + 1) + " <br><input type = 'text' name='ingredients[]'>";
			document.getElementById(divName).appendChild(newdiv);
			counter++;
		}
	}
</script>
							
<label>Instructions</label>
<div><textarea name="instructions" class="input-block-level" rows="5"></textarea></div>
							
<button type="submit" class="btn btn-primary">Submit</button>
</fieldset>
</form>
<a href="index.php">Return to recipe list</a>