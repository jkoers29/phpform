<?php
function render($page) {
    ob_start();
    include $page . '.php';
    $content = ob_get_clean();
    include 'layout.php';	
}


if(isset($_GET['action']) && $_GET['action'] == "insert")
{
	$current = 'form';
}
elseif($_SERVER['REQUEST_METHOD'] == "POST")
	$current = 'thanks';
else
	$current = 'list';
render($current);
?>