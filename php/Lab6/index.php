<?php
include_once "recipeModel.php";
include_once "view.php";

$recipeList = new RecipeModel();

if(isset($_GET['action']) && $_GET['action'] == "insert")
{
	$page = 'form';
	$data = null;
}
elseif($_SERVER['REQUEST_METHOD'] == "POST")
{
	$page = 'thanks';
	$data = null;
	$recipeList->insert($_POST['title'], $_POST['ingredient0'], $_POST['ingredient1'], $_POST['ingredient2'], $_POST['instructions']);
}
else
{
	$page = 'list';
	$data = $recipeList->findAll();
}
$view = new View($page, $data);
$view->render($view);
?>