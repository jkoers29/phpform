<?php
include_once "model.php";
include_once "connection.php";
class Recipe
{
	function __construct($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions)
	{
		$this->id = $id;
		$this->title = $title;
		$this->ingredient0 = $ingredient0;
		$this->ingredient1 = $ingredient1;
		$this->ingredient2 = $ingredient2;
		$this->instructions = $instructions;
	}
}

class RecipeModel extends Model
{
	function findAll()
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		$query = "Select * from recipe;";
		if($res = $conn->query($query))
		{
			$result = array();
			while($row = $res->fetch_assoc())
			{
				$result[] = new Recipe ($row['id'], $row['title'], $row['ingredient0'], $row['ingredient1'], $row['ingredient2'], $row['instructions']);			
			}
			return $result;
		}
	}

	function insert($title, $ing0, $ing1, $ing2, $inst)
	{
		$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if(mysqli_connect_errno())
		{
			die('Connect Error: ' . $conn->connect_errno);
		}
		$query = $conn->prepare("Insert Into recipe (title, ingredient0, ingredient1, ingredient2, instructions)  Values (?, ?, ?, ?, ?)");
		$query->bind_param('sssss', $title, $ing0, $ing1, $ing2, $inst);
		$query->execute();
	}
}